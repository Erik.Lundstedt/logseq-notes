;;; Directory Local Variables
;;; For more information see (info "(emacs) Directory Variables")

((org-mode . ((eval . (auto-save-mode nil))
			  (mode . org-logseq)
			  (mode . orgseq)
			  (mode . org-modern)
			  (mode . electric-pair)
			  (mode . abbrev))))
