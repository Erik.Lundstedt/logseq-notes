* awesome
** the awesome windowmanager is a tilling windowmanager for linux that is mostly written in, and configured in [[lua]]
** it can also be configured in [[fennel]] with a little bit of setup
** it has a builtin [[statusbar]], [[wallpaper-setter]] ,support for [[window-gaps]], [[notification-daemon]] and [[hotkey-daemon]] out of the box.
** because it is configured using code, rather than a data-storage-format like [[json]],[[toml]] and [[yaml]] it is highly configurable
* configuration
** on first run, awesome is configured to make windows float
** to run a ~dmenu_run~-style prompt, press  ~super+p~
** to open a terminal([[xterm]] by default) press ~super+return~
** to cycle over available layouts, press ~super+spacebar~
