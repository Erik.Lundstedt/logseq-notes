
# Kläder och utstyrsel 
-   [ ] Kläder 
    -   [ ] byxor X 4
    -   [ ] t-shirt X 4
-   [ ] Skor
-   [ ] Strumpor, underkläder

# Necessäär 
-   [ ] Tandborste
-   [ ] Tandkräm
-   [ ] Deodorant
-   [ ] Solkräm eller annat solskydd
-   [ ] Shampoo
-   [ ] Mediciner
-   [ ] Plåster

# Sovsalsgrejer 
-   [ ] Luftmadrass/annat liggunderlag
-   [ ] Sovsäck
-   [ ] Bekväm pyjamas (bra med två olika som passar beroende på temperatur i sovsal)

# Alltid bra att ha i ryggsäcken
-   [ ] Något att äta
-   [ ] Något att dricka (undvik för mycket läsk/kaffe!)
-   [ ] Någon form av fläkt (solfjäder etc)
-   [ ] Näsdukar (man vet aldrig vad man kan få för smuts,eller vilka kroppsvätskor som kan läcka!)
-   [ ] Mensprodukter (var inte rädd att fråga andra heller!)
-   [ ] Laddare till mobil
-   [ ] Närcons finfina häfte
-   [ ] Plåster

